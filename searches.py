def binSearch(l,
              el,
              valueL = lambda x: x,
              valueEl = lambda x: x,
              increasing = True,
              notFound = -1):
    """binSearch(l,
              el,
              [valueL] = lambda x: x,
              [valueEl] = lambda x: x,
              [increasing] = True,
              [notFound] = -1):
    
    l is the list in which the search will be performed.
    
    el is the searched element.
    
    [valueL] is a single parameter function that calculates the value of a
        given element in l.
        
    [valueEl] is a single parameter function that calculates the value the el
        parameter
        
    The search compares valueEl(el) to valueL(l[i]) to determine the relation
        between them. With this the function becomes more abstract.
        
    [increasing] is a flag to tell the function if the list is ordered
        increasingly or decreasingly.
        
    [notFound] is the return value if the element is not in the list.
    
    IMPORTANT: the list MUST be sorted by the criteria estabilished by the [valueL]
        function AND the [increasing] flag, or it will return inconsistent results.
    """
    
    targetValue = ( (-1)**(1 + increasing) ) * valueEl(el)
    def f(head, tail):
        if head > tail:
            return notFound
        else:
            m = (head + tail) / 2
            valueCurr = ( (-1)**(1 + increasing) ) * valueL(l[m])
            if valueCurr == targetValue:
                return m
            elif valueCurr > targetValue:
                return f(head, m - 1)
            else:
                return f(m + 1, tail)
    return f(0, len(l) - 1)
    

def seqSearch(l, el, valueL = lambda x: x, valueEl = lambda x: x, notFound = -1):
    """seqSearch(l, el, valueL = lambda x: x, valueEl = lambda x: x, notFound = -1):
    
    l is the list in which the search will be performed.
    
    el is the searched element.
    
    [valueL] is a single parameter function that calculates the value of a
        given element in l.
        
    [valueEl] is a single parameter function that calculates the value the el
        parameter
        
    The search compares valueEl(el) to valueL(l[i]) to determine the relation
        between them. With this the function becomes more abstract.
        
    [notFound] is the return value if the element is not in the list.
    
    """
    
    targetValue = valueEl(el)
    for i in xrange(len(l)):
        if targetValue == valueL(l[i]):
            return i
    return notFound
